import React ,{useState , useEffect} from 'react';
// import {Topheader , Sideheader} from '../component/Header';
// import Product from '../component/Product';
// import Bottomfooter from '../component/Footer';
import { NavLink , BrowserRouter, Route } from 'react-router-dom';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'; 
import BootstrapTable from 'react-bootstrap-table-next';
// import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory, { PaginationProvider, PaginationListStandalone ,SizePerPageDropdownStandalone , PaginationTotalStandalone } from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import 'react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css';
// import $ from 'jquery';
import { Button , Modal} from 'react-bootstrap';

class Home extends React.Component {
    constructor() {
        super();
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            product_id: '' ,
            errors:{},
            columns: [
            {
                dataField: 'name',
                text: 'Product Name',
                sort: true,
                formatter: (cell, row, index, extraData) => (
                    <div>
                        <FirstLetterUppper name={row.name} />
                    </div>
                ),
            }, {
                dataField: 'type',
                text: 'Product Type',
                sort: true
            }, {
                dataField: 'status',
                text: 'Product Status',
                sort: true,
                formatter: (cell, row, index, extraData) => (
                    <div>
                    {row.status == 1 ? <small className="badge badge-success"><i className="far fa-check-circle"></i> Available</small> :
                    <small className="badge badge-secondary"><i className="far fa-times-circle"></i> Unavailable</small>}
                    </div>
                ),
            }, {
                dataField: 'action',
                text: 'Action',
                sort: true,
                formatter: (cell, row, index, extraData) => (
                    <div>
                        <NavLink to={"/update-product/"+row.id}>
                            <button className="btn btn-sm btn-info mr-1 mb-1"><i className="fas fa-edit"></i> Edit</button>
                        </NavLink>
                        <NavLink to="/">
                            <button className="btn btn-sm btn-danger mb-1 btn-delete"
                                onClick={() => {
                                    let p_id = row.id;
                                    this.handleClick(p_id);
                                }}>
                                <i className="fas fa-trash-alt"></i> Delete
                            </button>
                        </NavLink>
                    </div>
                ),
            }],
            open: false
        };
        this.handleClick = this.handleClick.bind(this);
        this.onClose = this.onClose.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    handleClick(id) {
        this.setState({
            product_id: id,
            open: true
        });
    }

    onDelete(){
        let messages;
        this.setState({ errors: {}})
        const data = {
            product_id: this.state.product_id
        };

        fetch('http://dev.inventory.com/api/product/delete', {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        }).then((res) => { return res.json() })
        .then((data) =>  {
            // [this.updated.message] : data.errors.message
            if(data.hasOwnProperty('errors')){
                messages = data.errors.message
            }else if (data.hasOwnProperty('success')){

                messages = data
                fetch('http://dev.inventory.com/api/product')
                .then(res => res.json())
                .then(
                    (result) => {
                        this.setState({
                            isLoaded: true,
                            items: result.data,
                            open: false
                        });

                        console.log(this.state.items);
                    },
                    // Note: it's important to handle errors here
                    // instead of a catch() block so that we don't swallow
                    // exceptions from actual bugs in components.
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
                )
            }
            // console.log(messages);
            this.setState({ errors: messages})
            // console.log(this.state.errors);
        })
        .catch((err)=>console.log(err))
    }

    onClose(){
        this.setState({
            open: false
        });
    }

    componentDidMount() {

        fetch('http://dev.inventory.com/api/product')
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    isLoaded: true,
                    items: result.data

                });

                console.log(this.state.items);
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
                this.setState({
                    isLoaded: true,
                    product_tb:null,
                    error
                });
            }
        )
    }
    render() {
        const options = {
            custom: true,
            // paginationSize: 4,
            // pageStartIndex: 1,
            // firstPageText: 'First',
            prePageText: 'Back',
            nextPageText: 'Next',
            // lastPageText: 'Last',
            // nextPageTitle: 'First page',
            // prePageTitle: 'Pre page',
            // firstPageTitle: 'Next page',
            // lastPageTitle: 'Last page',
            showTotal: true,
            totalSize: this.state.items.length
        };
        const contentTable = ({ paginationProps, paginationTableProps }) => (
            <div className="card-body">
                <ToolkitProvider
                    keyField="id"
                    data={ this.state.items }
                    columns={ this.state.columns }
                    search
                >
                    {props => (
                        <div>
                            <div className="col-12 col-md-6">
                                <div className="row">
                                    <p className="pr-2 pt-1 m-0">Filter: </p>
                                    <SearchBar { ...props.searchProps  } />
                                </div>
                            </div>
                            <BootstrapTable { ...props.baseProps } { ...paginationTableProps } wrapperClasses="table-responsive"/>
                        </div>
                        )
                    }
                </ToolkitProvider>
                <div className="row">
                    <div className="col-12 col-md-6">
                        <div className="col-12 col-md-12 p-0">
                            <SizePerPageDropdownStandalone
                                { ...paginationProps }
                            />
                        </div>
                        <div className="col-12 col-md-12 p-0">
                            <PaginationTotalStandalone { ...paginationProps } />
                        </div>
                    </div>
                    <div className="col-12 col-md-6">
                        <PaginationListStandalone { ...paginationProps } style={{ float:'right' }}/>
                    </div>
                </div>
            </div>
        );
        return (
            <div className="content-wrapper">
                <section className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1>Home</h1>
                            </div>
                            <div className="col-sm-6">
                                <ol className="breadcrumb float-sm-right">
                                    <li className="breadcrumb-item active">Home</li>
                                </ol>
                            </div>
                        </div>
                    </div>{/* /.container-fluid */}
                </section>

                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {this.state.errors.hasOwnProperty('success') ?  
                                    <div className="alert callout callout-success">
                                        <h5>Successfully</h5>
                                        <p>{this.state.errors.success.message}</p>
                                    </div>
                                : ''}
                                <div className="card card-primary">
                                    <div className="card-header">
                                        <h3 className="card-title">Dashboard</h3>
                                    </div>
                                    <PaginationProvider
                                        pagination={
                                            paginationFactory(options)
                                        }
                                        >
                                        { contentTable }
                                    </PaginationProvider>
                                    <DeleteModal onClose={this.onClose} onDelete={this.onDelete} show={this.state.open}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }

    // componentDidUpdate (){
    //     // let product_tb = this.state.product_tb;
    //     console.log('work');
    //     let product_tb;
    //     product_tb = window.$("#product-list").DataTable();
    //     product_tb.destroy();
    //     // product_tb.destroy();
    //     // window.$("#product-list").DataTable().destroy();
    //     product_tb = window.$("#product-list").DataTable();
    //     console.log(product_tb.rows().data());
    // }
}

const { SearchBar } = Search;

const DeleteModal = props =>{
    const [open , setOpen] = useState(props.show);

    useEffect(() => {
        setOpen(props.show);
    }, [props.show]);

    return <div>
        <Modal show={open} onHide={props.onClose}>
            <Modal.Header closeButton>
                <Modal.Title>
                    Are you sure ?
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                Are you sure want to delete this Product ?
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={props.onClose}>
                    Close
                </Button>
                <Button variant="danger" onClick={props.onDelete}>
                    Delete
                </Button>
            </Modal.Footer>
        </Modal>
    </div>
}

const FirstLetterUppper = ({ name }) =>{
    // console.log(name);
    var str = name.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });

    return str;
}

export default Home;
import React from 'react';
import {Topheader , Sideheader} from '../component/Header';
import Product from '../component/Product';
import Bottomfooter from '../component/Footer';
import { NavLink , BrowserRouter, Route } from 'react-router-dom';
import queryString from 'query-string';
import $ from 'jquery';


class UpdateProduct extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);
        this.state = {
            product_id: props.match.params.id , 
            product_name: '' , 
            product_type: '' , 
            product_status: '',
            errors:{}
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value})
        console.log(this.state);
    }

    handleSubmit(event) {
        event.preventDefault();
        let messages;
        this.setState({ errors: {}})
        const data = {
            product_id: this.state.product_id,
            product_name: this.state.product_name , 
            product_type: this.state.product_type , 
            product_status: this.state.product_status,
        };
        // var params = [];
        console.log(data);
        // Object.keys(data).forEach(function(key) {
        //     console.log(key)
        //     console.log(data[key])
        //     params.appends(<li value={key}>{data[key]}</li>)
        // });
        // console.log(params);
        
        fetch('http://dev.inventory.com/api/product/update/edit?'+queryString.stringify(data) , {
            method: 'PUT',
            headers: {
                // 'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            // body: JSON.stringify(data),
        }).then((res) => { return res.json() })
        .then((data) =>  {
            // [this.updated.message] : data.errors.message
            if(data.hasOwnProperty('errors')){
                messages = data.errors.message
            }else if (data.hasOwnProperty('success')){
                messages = data
            }
            // console.log(messages);
            this.setState({ errors: messages})
            // console.log(this.state.errors);
        })
        .catch((err)=>console.log(err))

    }

    componentDidMount() {

        fetch('http://dev.inventory.com/api/product/'+this.state.product_id)
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    isLoaded: true,
                    product_name: result.data.name,
                    product_type: result.data.type,
                    product_status: result.data.status

                });

                // console.log(this.state.product_status);
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
                this.setState({
                    isLoaded: true,
                    product_tb:null,
                    error
                });
            }
        )
    }
    render() {
        return (
            <div className="content-wrapper">
                <section className="content-header">
                    <div className="container-fluid">
                        <div className="row mb-2">
                            <div className="col-sm-6">
                                <h1>Update Product</h1>
                            </div>
                            <div className="col-sm-6">
                                <ol className="breadcrumb float-sm-right">
                                    <li className="breadcrumb-item"><a href="#">Home</a></li>
                                    <li className="breadcrumb-item active">Update Product</li>
                                </ol>
                            </div>
                        </div>
                    </div>{/* /.container-fluid */}
                </section>

                <section className="content">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                                {this.state.errors.hasOwnProperty('success') ?  
                                    <div className="alert callout callout-success">
                                        <h5>Successfully</h5>
                                        <p>{this.state.errors.success.message}</p>
                                    </div>
                                : ''}
                                {this.state.errors.hasOwnProperty('product_exist') ?  
                                    <div className="callout callout-danger">
                                        <h5>Error</h5>
                                        <p>{this.state.errors.product_exist}</p>
                                    </div>
                                : ''}
                                <div className="card card-primary">
                                    <div className="card-header">
                                        <h3 className="card-title">Update Product</h3>
                                    </div>
                                    {/* /.card-header */}
                                    {/* form start */}
                                    <form role="form" onSubmit={this.handleSubmit}>
                                        <div className="card-body">
                                            <div className="form-group">
                                                <label htmlFor="product_name">Product Name</label>
                                                <input type="text" className="form-control" id="product_name" 
                                                name="product_name" placeholder="Product Name" value={this.state.product_name} onChange={this.handleChange}/>
                                                <span id="error-message">{this.state.errors.hasOwnProperty('product_name') ? this.state.errors.product_name : ''}</span>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="product_type">Product Type</label>
                                                <input type="text" className="form-control" id="product_type" 
                                                name="product_type" placeholder="Product Type" value={this.state.product_type} onChange={this.handleChange}/>
                                                <span id="error-message">{this.state.errors.hasOwnProperty('product_type') ? this.state.errors.product_type : ''}</span>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="product_status">Product Status</label>
                                                <div className="form-check">
                                                    <label className="form-check-label">
                                                        <input className="form-check-input" type="radio" name="product_status" 
                                                        id="available" value="1" onChange={this.handleChange} checked={this.state.product_status == '1' ? true : ''} />
                                                        Available
                                                    </label>
                                                </div>
                                                <div className="form-check">
                                                    <label className="form-check-label">
                                                        <input className="form-check-input" type="radio" name="product_status" 
                                                        id="unavailable" value="0" onChange={this.handleChange} checked={this.state.product_status == '0' ? true : ''} />
                                                        Unavailable
                                                    </label>
                                                </div>
                                                <span id="error-message">{this.state.errors.hasOwnProperty('product_status') ? this.state.errors.product_status : ''}</span>
                                            </div>
                                        </div>
                                        {/* /.card-body */}
                                        <div className="card-footer text-right">
                                            <NavLink to="/"><button type="button" className="btn btn-secondary mr-1">Back</button></NavLink>
                                            <button type="submit" className="btn btn-primary">Submit</button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>

            </div>
        )
    }
}

export default UpdateProduct;
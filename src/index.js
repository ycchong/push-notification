import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap';
import '@fortawesome/fontawesome-pro/css/all.min.css';
import "admin-lte/dist/css/adminlte.min.css";
import $ from 'jquery';
window.jQuery = window.$ = $;
window.jQuery = $;
window.$ = $;
global.jQuery = $;
require("admin-lte");

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

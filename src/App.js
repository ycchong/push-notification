import React from 'react';
import Topheader from './component/Header';
import Sideheader from './component/Sidebar';
import Bottomfooter from './component/Footer';
import Product from './component/Product';
import Home from './pages/home';
import { BrowserRouter, Route } from 'react-router-dom';
import UpdateProduct from './pages/UpdateProduct';
import ReactNotifications from 'react-browser-notifications';
import Websocket from 'react-websocket';
import Echo from 'laravel-echo';
import serviceWorker from './serviceWorker';
import {registerServiceWorker, sendNotification,} from "./push-notifications";
// import PushNotificationDemo from "./pages/default.js";

window.Pusher = require('pusher-js');

class App extends React.Component {

  constructor(props) {
      super(props);
      // console.log(props);
      this.state = {
        error: null,
        isLoaded: false,
        errors:{},
        notification: [],
      };
      this.showNotifications = this.showNotifications.bind(this);
      this.handleClick2 = this.handleClick2.bind(this);
      this.handleData = this.handleData.bind(this);
  }

  handleData(data) {
    let result = data;
    console.log(result);
    // this.setState({count: this.state.count + result});
  }

  showNotifications() {
    if(this.n.supported()) this.n.show();
  }

  handleClick2(event) {
    window.focus()
    this.n.close(event.target.tag);
  }

  fetchNotification(){
    fetch('http://dev.notification.com/api/message' , {
      method: 'GET',
      headers: {
          // 'Accept': 'application/json',
          'Content-Type': 'application/json'
      },
    })
    .then(res => res.json())
    .then(
        (result) => {
            this.setState({
                isLoaded: true,
                notification: result.data

            });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
            this.setState({
                isLoaded: true,
                product_tb:null,
                error
            });
        }
    )
  }

  componentDidMount(){
    
    this.handleData();
    this.fetchNotification();

    if ("serviceWorker" in navigator && "PushManager" in window) {
      window.addEventListener('load', function() {
        navigator.serviceWorker.register('/sw.js');
      });
    }

    const options = new Echo({
      broadcaster: 'pusher',
      key: 'ABCDEFG',
      cluster: 'mt1',
      // encrypted: true
      wsHost: 'dev.notification.com',
      wsPort: 6001,
      // disableStats: true,
    }).channel('home').listen('NewMessage' , (e) => {
      this.setState({
        notification : e.message
      });

      Notification.requestPermission().then(function(result){
        if (result !== "granted") {

        }else{
          if(e.message.constructor === Array){
            e.message.map((index, i) => {
              const img = "logo192.png";
              const text = index.message;
              const title = index.title;
              const options = {
                body: text,
                icon: "logo512.png",
                vibrate: [200, 100, 200],
                tag: index.message_id,
                image: img,
                badge: "https://spyna.it/icons/android-icon-192x192.png",
                actions: [{ action: "Detail", title: "View", icon: "https://via.placeholder.com/128/ff0000" }]
              };
              navigator.serviceWorker.ready.then(function(serviceWorker) {
                serviceWorker.showNotification(title, options);
              });
            });
          }
            
        }
      });
    });
    
  }
  
  render (){
    return (
      <div className="wrapper">
        <BrowserRouter>
          <React.Fragment>
            <Topheader />
            <Sideheader/>
            <Route exact path='/' component={Home} />
            <Route exact path='/product' component={Product} />
            <Route exact path='/update-product/:id' component={UpdateProduct} />
            {/* <Websocket url='ws://dev.notification.com:6001/app/ABCDEFG?protocol=7&client=js&version=5.0.3&flash=false' onMessage={this.handleData} onOpen={console.log('server connecting')} debug={true} /> */}
            <Bottomfooter />
            {/* <div className="text-center"><PushNotificationDemo /></div> */}
          </React.Fragment>
        </BrowserRouter>
      </div>
    )
  }
}


export default App;

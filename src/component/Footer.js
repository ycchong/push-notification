import React from 'react';
import { NavLink } from 'react-router-dom';

const Bottomfooter = props => (
    <footer className="main-footer">
        {/* <div className="float-right d-none d-sm-block">
            <b>Version</b> 3.0.0-rc.1
        </div> */}
        <strong>Copyright © {(new Date().getFullYear())} <NavLink to="/">Gigabit Hosting Sdn Bhd</NavLink>.</strong> All rights
        reserved.
    </footer>

);

export default Bottomfooter;
import React from 'react';
import { NavLink } from 'react-router-dom';
// import { matchPath } from "react-router";

const Topheader = props => (
    <nav className="main-header navbar navbar-expand navbar-white navbar-light">
        <ul className="navbar-nav">
            <li className="nav-item">
                <NavLink className="nav-link" data-widget="pushmenu" to="/"><i className="fas fa-bars" /></NavLink>
            </li>
            {/* <li className="nav-item d-none d-sm-inline-block">
                <NavLink to="../../index3.html" className="nav-link">Home</NavLink>
            </li>
            <li className="nav-item d-none d-sm-inline-block">
                <NavLink to="#" className="nav-link">Contact</NavLink>
            </li> */}
        </ul>
        {/* <form className="form-inline ml-3">
            <div className="input-group input-group-sm">
                <input className="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search" />
                <div className="input-group-append">
                    <button className="btn btn-navbar" type="submit">
                    <i className="fas fa-search" />
                    </button>
                </div>
            </div>
        </form> */}
        <ul className="navbar-nav ml-auto">
            <li className="nav-item dropdown">
                <NavLink className="nav-link" data-toggle="dropdown" to="/mail">
                    <i className="far fa-bell" />
                    <span className="badge badge-warning navbar-badge">15</span>
                </NavLink>
                <div className="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span className="dropdown-item dropdown-header">15 Notifications</span>
                    <div className="dropdown-divider" />
                    <NavLink to="mail" className="dropdown-item">
                    <i className="fas fa-envelope mr-2" /> 4 new messages
                    <span className="float-right text-muted text-sm">3 mins</span>
                    </NavLink>
                    <div className="dropdown-divider" />
                    <NavLink to="mail" className="dropdown-item">
                    <i className="fas fa-users mr-2" /> 8 friend requests
                    <span className="float-right text-muted text-sm">12 hours</span>
                    </NavLink>
                    <div className="dropdown-divider" />
                    <NavLink to="mail" className="dropdown-item">
                    <i className="fas fa-file mr-2" /> 3 new reports
                    <span className="float-right text-muted text-sm">2 days</span>
                    </NavLink>
                    <div className="dropdown-divider" />
                    <NavLink to="mail" className="dropdown-item dropdown-footer">See All Notifications</NavLink>
                </div>
            </li>
            <li className="nav-item dropdown">
                <NavLink className="nav-link" data-toggle="dropdown" to="/mail">
                    <i className="fas fa-user-alt"></i>
                </NavLink>
                <div className="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span className="dropdown-item dropdown-header">Profile</span>
                    <div className="dropdown-divider" />
                    <NavLink to="mail" className="dropdown-item">
                        <i className="fas fa-cog mr-2"></i> Setting
                    </NavLink>
                    <div className="dropdown-divider" />
                    <NavLink to="mail" className="dropdown-item">
                        <i className="fas fa-sign-out-alt mr-2"></i> Logout
                    </NavLink>
                </div>
            </li>
        </ul>
    </nav>

    
);

export default Topheader;
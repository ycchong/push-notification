import React from 'react';
import { NavLink } from 'react-router-dom';
import { matchPath } from "react-router";

class Sideheader extends React.Component {
    constructor() {
        super();
        this.state = {
            url : window.location.pathname,
        };
    }

    render(){
        return(
            <aside className="main-sidebar sidebar-dark-primary elevation-4">
                <NavLink to="/" className="brand-link">
                    <img src={require('admin-lte/dist/img/AdminLTELogo.png')} alt="AdminLTE Logo" className="brand-image img-circle elevation-3" style={{opacity: '.8'}} />
                    <span className="brand-text font-weight-light">INVENTORY</span>
                </NavLink>
                <div className="sidebar">
                    <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div className="image">
                        <img src={require('admin-lte/dist/img/user2-160x160.jpg')} className="img-circle elevation-2" alt="User Image" />
                    </div>
                    <div className="info">
                        <NavLink to="/" className="d-block">User</NavLink>
                    </div>
                    </div>
                    <nav className="mt-2">
                        <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li className={"nav-item has-treeview " + (this.state.url == '/' || this.state.url == '/product' || productUpdate != null ? 'menu-open' : {})}>
                            <NavLink to="#" className="nav-link" isActive={checkActive}>
                            <i className="nav-icon fas fa-tachometer-alt" />
                            <p>
                                Dashboard
                                <i className="right fas fa-angle-left" />
                            </p>
                            </NavLink>
                            <ul className="nav nav-treeview">
                            <li className="nav-item">
                                <NavLink to="/" exact className="nav-link" isActive={checkHomeActive}>
                                    <i className="far fa-circle nav-icon"/>
                                    <p>Home</p>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="/product" exact className="nav-link" isActive={isActive.bind(this, '/product')}>
                                <i className="far fa-circle nav-icon" />
                                <p>Product</p>
                                </NavLink>
                            </li>
                            </ul>
                        </li>
                        {/* <li className="nav-item">
                            <NavLink to="../widgets.html" className="nav-link">
                            <i className="nav-icon fas fa-th" />
                            <p>
                                Widgets
                                <span className="right badge badge-danger">New</span>
                            </p>
                            </NavLink>
                        </li> */}
                        </ul>
                    </nav>
                </div>
            </aside>
        )
    }
}

const productUpdate = matchPath(window.location.pathname , {
    path: "/update-product/:id",
    exact: true,
    strict: true
});
// console.log(productUpdate);

const checkActive = (match, location) => {
    //some additional logic to verify you are in the home URI
    if(!location) return false;
    const {pathname} = location;
    // console.log(matchPath(pathname));
    const productUpdate = matchPath(pathname , {
        path: "/update-product/:id",
        exact: true,
        strict: true
    });
    
    if(pathname === "/" || pathname === "/product" || productUpdate !== null) 
    return pathname;
}

const checkHomeActive = (match, location) => {
    //some additional logic to verify you are in the home URI
    if(!location) return false;
    const {pathname} = location;
    // console.log(matchPath(pathname));
    const productUpdate = matchPath(pathname , {
        path: "/update-product/:id",
        exact: true,
        strict: true
    });
    
    if(pathname === "/" || productUpdate !== null) 
    return pathname;
}

const isActive = (path, match, location) => !!(match || path === location.pathname);

export default Sideheader;
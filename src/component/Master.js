import React from 'react';
import {Topheader , Sideheader} from './Header';
import Centercontent from './Product';
import Bottomfooter from './Footer';
// import { NavLink , BrowserRouter, Route } from 'react-router-dom';

const Master = props => (
    <div className="wrapper">
        <Topheader />
        <Sideheader />
        <Centercontent />
        <Bottomfooter />
    </div>
);

export default Master;